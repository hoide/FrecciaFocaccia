```
===========================================================
                                                           
    /|   /|   /|   /|      /|       /|   /|   /|   /|      
   | |  | |  | |  | |     | |      | |  | |  | |  | |      
---|F---|o---|c---|a------|?-------|c---|i---|a---|!--->>> 
   | |  | |  | |  | |     | |      | |  | |  | |  | |      
   |/   |/   |/   |/      |/       |/   |/   |/   |/       
                                                           
===========================================================
```
# FrecciaFocaccia

This package is a light-weight pixel testbeam analysis tool.

## ChangeLog
* 2017-03-26:
  * More consistent calculation of the intersection position of tracks to planes. Better performing in case of larger tilt angles
  * Deprecated `AlignTracking` and separated to `Alignment` and `Tracking`.
  * Implemented alignment result dumping/loading using the `JSON` format.
  * Refined `TrackAnaExample`.
  * Minor improvements.
* 2017-03-19:
  * Improvements in `AlignTracking` algorithms and other many small improvements.
* 2017-03-09:
  * Added a data converter from the Eudaq output (v1.7-dev compatible).
  * Refreshed the clustering algorithm.
  * Error scale tuning is added (optionally)
* 2017-03-06: Just born.

## Requirements:
* g++ 4.9 (C++11) or LLVM version 8.0.0
* ROOT v5.34/18 or later pre-installed (installed with C++11 enabled).

Operation confirmed in both Lxplus (CERN) and Mac OS X (10.12.3).

Please make sure ROOT libraries are picked up in `LD_LIBRARY_PATH` or `DYLD_LIBRARY_PATH`,
and the following commands works (used in the `Makefile`:
* root-config --cflags
* root-config --libs

## Table of contents
* [Installation](#installation)
  * [Run a demo working out of the box](#run-a-demo-working-out-of-the-box)
* [General features](#general-features)
  * [Analyses](#analyses)
  * [Can and cannot](#can-and-cannot)
  * [Configuration](#configuration)
* [Data format](#data-format)
  * [Input Raw format](#input-raw-format)
  * [Cluster data format](#cluster-data-format)
  * [Track data format](#track-data-format)
* [Geometry](#geometry)
  * [General](#general)
  * [How to specify the geometry](#how-to-specify-the-geometry)
  * [Plane ID](#plane-id)
  * [Front-ends](#front-ends)
  * [FlipRot matrix](#fliprot-matrix)
  * [Error scales](#error-scales)
* [Alignment correction convention](#alignment-correction-convention)
* [Analysis framework](#analysis-framework)
* [RawAnalysis](#rawanalysis)
  * [HotPixelAnalysis](#hotpixelanalysis)
  * [Clustering](#clustering)
* [ClusterAnalysis](#clusteranalysis)
  * [MaskMap](#maskmap)
  * [Correlator](#correlator)
  * [Alignment](#alignment)
  * [Tracking](#tracking)
* [TrackAnalysis](#trackanalysis)
  * [TrackAnaExamlpe](#trackanaexample)
* [Algorithm detail in Alignment](#algorithm-detail-in-alignment)
  * [Pre-alignment](#pre-alignment)
  * [Track finding](#track-finding)
  * [Track fitting](#track-fitting)
  * [Alignment procedure](#alignment-procedure)

## Installation
Nothing special is required. Use the Makefile.
```
git clone https://${USER}@gitlab.cern.ch/hoide/FrecciaFocaccia.git
```
then
```bash
cd FrecciaFocaccia
make -j
```

### Run a demo working out of the box
Try to use `demo.sh` after installation:
```bash
./demo.sh
```
It downloads an example [RAW root file](#input-raw-format) and then process the reconstruction chain.

***

# General features
This package offers a simple reconstruction of test beam data.
The input format of this analysis tool is a generic flat `TTree`, and hence
there is no other library dependency except ROOT.

So far the main supposed use-case is analysis of the Eutelescope dataset, 
and an external data converter is needed to create the input Ntuple.
Once such a converter is provided, the tool may be potentially used for other data.

The analysis process is mainly divided into 3 steps depending on the input data type:
* **EudaqConv**       : convert the Eudaq output ROOT file to focaccia RAW file format.
* **RawAnalysis**     : using the pixel-hit information as input. (e.g. Clustering)
* **ClusterAnalysis** : using the cluster data information as input. (e.g. Alignment)
* **TrackAnalysis**   : using the reconstructed track information as input (e.g. Efficiency)

It is expected that the above analyses are processed sequentially for each dataset.

The execution command is simple enough like:
```
./bin/focaccia -c config/GenovaAug2016.json -a ClusterAnalysis -i output/clusters.root 
```

The full usage is very simple.
```
./bin/focaccia --help
focaccia: a light-weight telescope data analyser.

  --config   : [config_file]
  --analysis : [analysis_type] available: RawAnalysis, ClusterAnalysis, TrackAnalysis
  --input    : [input_file]
  --help     : Print helps
  --verbose  : verbose level. 0 = muted (default), 1 = info, 2 = debug, 3 = verbose
  --batch    : optimized printout for batch mode
  -c         : same as --config
  -a         : same as --analysis
  -i         : same as --input
  -h         : same as --help
  -v         : same as --verbose
  -b         : same as --batch

```

## Analyses
So far the following analyses are implemented.
The time is an estimation using intel Core i7 2.2GHz (single core).
* RawAnalysis:
  * [**HotPixelAnalysis**](#hotpixelanalysis) : Finding noisy pixels using occupancy map (~6k events/sec)
  * [**Clustering**](#clustering) : perform clustering (~800 events/sec)
* ClusterAnalysis:
  * [**MaskMap**](#masmMap) : Perform further noise rejection at the cluster level (~13k events/sec)
  * [**Correlator**](#correlator) : Extract a rough x-y adjustment by taking the correlation of positions with respect to the reference plane. (~1k events/sec. Usually truncation at O(1000) events is fine)
  * [**Alignment**](#alignment) : Track finding, alignment and store the information of cluster properties of good tracks (tracking: ~400 events/sec, alignment: several min)
  * [**Tracking**](#tracking) : Similar to [Alignment](#alignment), but without alignment calculation. Loading the alignment constant and derive all tracks.
* TrackAnalysis:
  * [**TrackAnaExample**](#trackanaexample) : An example of analysing the output of Tracking. (~10k events/sec)

## Can and cannot
* Speciality of the edge pixel's geometry is not handled.
* Cluster information lacks the raw hit informations at the output.
* Cluster position is estimated using the weigthed mean of ToTs. 
* ToT --> charge conversion is not implemented yet.
* Hit requirement to tracks as well as track finding acceptance is adjustable by config.
* Tracks are fitted as straight lines. No multipe scattering, no magnetic fields.
* Alignment degrees of freedom is adjustable by config for each cycle.
* Track error scaling tuning procedure is introduced. (*need to study the behavior*)

## Configuration
Configuration of geometry and analysis parameters is controlled by a [JSON](http://www.json.org) input file, 
and [`nlohmann::json`](https://github.com/nlohmann/json) is used to comprehend it.

See [config/GenovaAug2016.json](https://gitlab.cern.ch/hoide/FrecciaFocaccia/blob/master/config/GenovaAug2016.json) for example.

For editing `JSON` format, you can use either a text editor, or assistance e.g. [JSON Editor Online](http://www.jsoneditoronline.org).

```javascript
{
    
    "geometry" : {
	"0":{ "plane":0,  "type":"telescope", "FE":"mimosa26",	    "zpos":  0.0, "fliprot":[ 1, 0, 0, 1 ], "error_scale":[1.0,1.0] },
	"1":{ "plane":1,  "type":"telescope", "FE":"mimosa26",	    "zpos": 83.0, "fliprot":[ 1, 0, 0, 1 ], "error_scale":[1.0,1.0] },
	"2":{ "plane":2,  "type":"telescope", "FE":"mimosa26",	    "zpos":164.0, "fliprot":[ 1, 0, 0, 1 ], "error_scale":[1.0,1.0] },
	"3":{ "plane":23, "type":"dut",       "FE":"fei4",		    "zpos":280.0, "fliprot":[ 0,-1, 1, 0 ], "error_scale":[1.0,1.0] },
	"4":{ "plane":22, "type":"dut",       "FE":"fei4_100x25",	"zpos":330.0, "fliprot":[ 0,-1, 1, 0 ], "error_scale":[1.0,1.0] },
	"5":{ "plane":21, "type":"dut",       "FE":"fei4_50x50",	"zpos":370.0, "fliprot":[ 0,-1, 1, 0 ], "error_scale":[1.0,1.0] },
	"6":{ "plane":20, "type":"dut",       "FE":"fei4",          "zpos":460.0, "fliprot":[ 1, 0, 0, 1 ], "error_scale":[1.0,1.0] },
	"7":{ "plane":3,  "type":"telescope", "FE":"mimosa26",      "zpos":527.0, "fliprot":[ 1, 0, 0, 1 ], "error_scale":[1.0,1.0] },
	"8":{ "plane":4,  "type":"telescope", "FE":"mimosa26",      "zpos":612.0, "fliprot":[ 1, 0, 0, 1 ], "error_scale":[1.0,1.0] },
	"9":{ "plane":5,  "type":"telescope", "FE":"mimosa26",      "zpos":695.0, "fliprot":[ 1, 0, 0, 1 ], "error_scale":[1.0,1.0] }
    }
}
```

***

# Data format
## Input RAW format
The input is a flat ntuple with the following variables:
* **euEvt** (`int`) : Event number
* **iden** (`vector<int>`) : Sensor plane ID
* **col** (`vector<int>`) : pixel hit position column
* **row** (`vector<int>`) : pixel hit position row
* **tot** (`vector<int>`) : pixel hit time-over-threshold (ToT)
* **lv1** (`vector<int>`) : pixel timing

```
root [2] tree->Print()
******************************************************************************
*Tree    :tree      : tree                                                   *
*Entries :    46642 : Total =       718576915 bytes  File  Size =   47314678 *
*        :          : Tree compression factor =  15.22                       *
******************************************************************************
*Br    0 :euEvt     : euEvt/I                                                *
*Entries :    46642 : Total  Size=     187474 bytes  File Size  =      65667 *
*Baskets :        6 : Basket Size=      51200 bytes  Compression=   2.85     *
*............................................................................*
*Br    1 :iden      : vector<int>                                            *
*Entries :    46642 : Total  Size=   89877131 bytes  File Size  =    2514342 *
*Baskets :     1791 : Basket Size=    3201536 bytes  Compression=  35.73     *
*............................................................................*
*Br    2 :col       : vector<int>                                            *
*Entries :    46642 : Total  Size=   89875336 bytes  File Size  =   22817922 *
*Baskets :     1791 : Basket Size=    3201536 bytes  Compression=   3.94     *
*............................................................................*
*Br    3 :row       : vector<int>                                            *
*Entries :    46642 : Total  Size=   89875336 bytes  File Size  =   15161877 *
*Baskets :     1791 : Basket Size=    3201536 bytes  Compression=   5.93     *
*............................................................................*
*Br    4 :tot       : vector<int>                                            *
*Entries :    46642 : Total  Size=   89875336 bytes  File Size  =    1742960 *
*Baskets :     1791 : Basket Size=    3201536 bytes  Compression=  51.54     *
*............................................................................*
*Br    5 :lv1       : vector<int>                                            *
*Entries :    46642 : Total  Size=   89875336 bytes  File Size  =    1591000 *
*Baskets :     1791 : Basket Size=    3201536 bytes  Compression=  56.47     *
*............................................................................*
```

## Cluster data format

## Track data format
The output "trackTree" ntuple is not event-based, but track-based (i.e. 1 entry/track).
The hits on the track is stored in the entry.
(The telescope hit informations are dropped except `nhits_tel`.)

* **chi2** (`float`) : track chi2
* **nhits** (`int`) : total number of hits on the track
* **nhits_tel** (`int`) : total number of telescope hits on the track
* **plane** (`vector<int>`) : plane ID
* **x** (`vector<double>`) : global x position of the track on the plane
* **y** (`vector<double>`) : global y position of the track on the plane
* **local_x** (`vector<double>`): local x position of the track on the plane (i.e. column-direction)
* **local_y** (`vector<double>`): local y position of the track on the plane (i.e. row-direction)
* **trkcol** (`vector<double>`): the column of the track that the track is pointing
* **trkrow** (`vector<double>`): the row of the track that the track is pointing
* **folded_x** (`vector<double>`): the folded local x position of the track on the plane. Mirror-flipped for left/right-side of the double-column edge.
* **folded_y** (`vector<double>`): the folded local y position of the track on the plane.
* **ave_col** (`vector<double>`): the ToT-weighted column position of the cluster on the track.
* **ave_row** (`vector<double>`): the ToT-weighted row position of the cluster on the track.
* **size** (`vector<int>`): the size of the cluster.
* **sumToT** (`vector<double>`): the sum of the ToT of the cluster
* **res_x** (`vector<double>`): the x residual of the cluster with respect to the track on the plane.
* **res_y** (`vector<double>`): the y residual of the cluster with respect to the track on the plane.


# Geometry
## General
The user must provide the valid geometry condition of the corresponding dataset.

Two coordinate systems, referred to as **global** and **local** systems are used in this analysis.
* The **global** coordinate system is a right-handed system and __unique__.
  * The **origin** of the system is usually taken at the center of the *Plane 0* (but z-offset is adjustable). 
  * The **z-axis** is along the beam from upstream to downstream
  * The **y-axis** is vertical, from bottom to top.
  * The **x-axis** is taken horizontally.
* The **local** coordinate system is a right-handed system, and is associated for each sensor.
  * The **local x-axis** is taken along the *column* direction in ascending order.
  * The **local y-axis** is taken along the *row* direcction in ascending order.
  * The **origin** of the local coordinate system is the center of the sensor. For example, in case of the FEI4, the origin is at the left-bottom corner of the pixel (40, 168).
  * It is assumed that both column and row are **counted from 0** for all sensors.
  * By **default** the local x and y are along the global x and y.

## How to specify the geometry
The telescope geometry is specified by feeding minimal informations on the telescope arrangement, including:
* Dummy ID (to define the sequence of process)
* Plane ID (to be consistent with the RAW input data)
* Sensor type
* Frontend type
* Estimated z position in the global coordinate
* The "FlipRot" matrix whch specifies the orientation of the sensor which is not easy to correct
* Error scale vector in the position resolution (used to re-weight the track chi2 on each plane)

An example of the specification of one plane is as follows:
```javascript
	"3":{ "plane":23, "type":"dut",       "FE":"fei4",		    "zpos":280.0, "fliprot":[ 0,-1, 1, 0 ], "error_scale":[1.0,1.0] },
```
In this example:
* Dummy ID = 3
* Plane ID = 23
* Sensor type = "dut" (**case-sensitive** : to be improved)
* Frontend type = "fei4" (**case-sensitive** : to be improved)
* z position = 280 mm
* FlipRot matrix = `[0, -1, 1, 0 ]`
* Error scale = `[1.0, 1.0]`

Note: **millimeter** is consistently used as the scale unit throughout the package.

### Plane ID
The plane ID is an unsigned integer. In case of Eutelescope, the convention is:
* **Telescope** sensors: use ID starting from **0** from upstream to downstream 
* **DUT** sensors: use ID starting from **20**
The plane ID does not need to be consistent with the z-position, 
but must be consistent with the ID written in the RAW data.

The classification of the plane (telescope/dut) must be specified in the `"type"` field.

### Front-ends
The field `"FE"` is allocated to specify the type of front-ends.
So far the following sensors are supported.
* **mimosa26**
* **fei4**
* **fei4_50x50** : a special FEI4 readout for 50um(col) x 50um(row) pixel sensor pitch
* **fei4_100x25** : a special FEI4 readout for 100um(col) x 25um(row) pixel sensor pitch

The above names are **case-sensitive**. By telling the front-ends of each sensor, 
the geometry class `geo` is able to map front-end specific geometry functions, 
e.g. telling the width of the column.

### FlipRot Matrix
The "FlipRot" matrix is necessary to be specified in the configuration 
in order to arrange the local coordinate system of each plane to be approximately accurate. 
It is applied on each plane specified in the geometry configuration for each file gives the rough orientation and parity of the sensor.
The matrix `[a, b, c, d]` is supposed to be a 2x2 matrix applied on the local coordinate system:
```math
\begin{pmatrix}x_{L}' \\ y_{L}'\end{pmatrix} = \begin{pmatrix}a & b\\ c & d\end{pmatrix}\begin{pmatrix}x_{L} \\ y_{L}\end{pmatrix}
```
By default the local coordinate is along the global coordinate.

Some basic trivial examples of specification of the FlipRot matrix:
* The column direction is flipped

```math
\begin{pmatrix}-1 & 0\\ 0 & 1\end{pmatrix}
```

* Rotation of the chip by 90 degree

```math
\begin{pmatrix}0 & -1\\ 1 & 0\end{pmatrix}
```

Some notes:
* The unitarity of the FlipRot matrix is not checked, and it is user's responsibility to ensure its validity.
* Only operations in the local $`(x, y)`$ plane is considered (no local z-axis)
* The **tilt** angle is not supposed to be corrected in this matrix. This should be considered in the alignment correction (see next).

## Error scales
The **error scale** is introduced to adjust the resolution of clusters for each plane. Physically this includes both of the intrinsic cluster resolution mismatch and other effects like multiple scattering and misalignments. With this tune, the track $`\chi2`$ distribution agreement becomes better.

# Alignment correction convention
For a given local coordinate position of $`\begin{pmatrix} x \\ y \end{pmatrix}_{L}`$,
the mapping to the global coordinate system is performed as follows:
```math
\begin{pmatrix}x \\ y \\ z\end{pmatrix}_{G} = \begin{pmatrix}T_{x} \\ T_{y} \\ z_{G}\end{pmatrix}+R_{z}R_{y}R_{x}S \begin{bmatrix}\begin{pmatrix}T_{x}^{0} \\ T_{y}^{0} \\ 0\end{pmatrix} + F \begin{pmatrix}x \\ y \\ 0\end{pmatrix}_{L}\end{bmatrix}
```
where $`(T_{x}^{0}, T_{y}^{0})`$ is the pre-alignment translation, and $`F`$ is the `FlipRot` matrix (described before), and the rest operators are defined as follows:
```math
S =\begin{pmatrix} 1+S_{x} &  & \\  & 1+S_{y} & \\ & & 1\end{pmatrix},
R_{x} = \begin{pmatrix}1 & & \\ & \cos\theta_{x} & \sin\theta_{x} \\ & -\sin\theta_{x} & \cos\theta_{x} \end{pmatrix}
R_{y} = \begin{pmatrix}\cos\theta_{y} & & -\sin\theta_{y} \\ & 1 & \\ \sin\theta_{y} & & \cos\theta_{y}\end{pmatrix}
R_{z} = \begin{pmatrix} \cos\theta_{z} & \sin\theta_{z} & \\ -\sin\theta_{z} & \cos\theta_{z} & \\  &  & 1\end{pmatrix}
```
The suffices $`{G, L}`$ represent if that coordinate is associated to global or local coordinates respectively.

Note that the rotation operation are performed around the **global** coordinate axis (and not around the intermediate local coordinate axes during rotation steps) of each label.

The operator $`S`$ is for potentialy scale adjustment.


The parameters determined by the alignment process are:
```math
\vec{a} = \{ (T_{x}, T_{y}), (\theta_{x}, \theta_{y}, \theta_{z}), (S_{x}, S_{y}) \}
```

### Initial alignment correction
Initial alignment correction (not pre-alignment, but fine alignment) is possible to feed via the config file. An example of specification is as follows:

```javascript
    "geometry_fine_alignment" : {
        "0" :{ "trans":[ 0.00e+0, 0.00e+0], "rot":[ 0.00e+0,  0.00e+0,  0.00e+0], "scale":[ 0.00e+0,  0.00e+0] },
        "1" :{ "trans":[ 7.41e-3,-1.09e-2], "rot":[ 1.02e-2, -6.61e-3, -5.20e-3], "scale":[ 7.70e-5,  1.50e-4] },
        "2" :{ "trans":[-4.52e-3, 6.18e-3], "rot":[-1.35e-3,  2.88e-2,  2.79e-3], "scale":[-5.70e-5, -8.44e-5] },
        "23":{ "trans":[-1.21e-2, 2.94e-2], "rot":[-3.26e-2,  3.76e-2,  1.06e-2], "scale":[ 1.07e-4,  5.38e-3] },
        "22":{ "trans":[-3.59e-2, 8.71e-2], "rot":[-1.23e-2,  4.00e-2,  4.75e-2], "scale":[ 1.00e-4,  6.39e-3] },
        "21":{ "trans":[-1.56e-3, 1.40e-2], "rot":[ 3.96e-3,  1.05e-1,  2.54e-3], "scale":[ 8.84e-4,  6.50e-3] },
        "20":{ "trans":[ 8.35e-3, 2.31e-2], "rot":[ 0.00e+0,  0.00e+0,  7.60e-3], "scale":[ 7.77e-3,  6.75e-3] },
        "3" :{ "trans":[-1.05e-2, 6.17e-3], "rot":[ 0.00e+0,  0.00e+0,  4.63e-3], "scale":[ 2.47e-4,  2.08e-4] },
        "4" :{ "trans":[ 1.44e-3,-9.56e-3], "rot":[ 1.50e-2,  1.27e-2, -4.31e-3], "scale":[ 5.54e-5,  9.58e-5] },
        "5" :{ "trans":[ 8.58e-4, 7.75e-4], "rot":[ 7.85e-3,  2.41e-2,  1.68e-3], "scale":[-3.76e-5, -7.41e-5] }
    },
```


# Analysis framework
The process is controlled by the command option of the main executable `focaccia` and the configuration file.

A typical example of execution of the command is like:
```c++
./bin/focaccia -c config/GenovaAug2016.json -a ClusterAnalysis -i output/clusters.root 
```

The executable `focaccia` requires 3 input parameters:
* configuration file (with `-c` or `--config` option)
* analysis type (either of {"EudaqConv", "RawAnalysis", "ClusterAnalysis, "TrackAnalysis"} with `-a` or `--analysis` option)
* input file (with `-i` or `--input` option)

The role of the `main` function of the executable `focaccia` is just to parse these user's input and forward to a static function `IAnalysis::play()`:
```c++
class IAnalysis {
public:
  static void play( const config& c, const std::string& ana_type, const std::string& input_file );
  ...
};
```

`IAnalysis::play()` interprets the contents of the configuration file, and it will know which analyses need to be performed.

For example, in case of performing `RawAnalysis`, `IAnalysis::play()` will look for the following fragment inside the configuration file:
```javascript
    "RawAnalysis" : [

	{ "HotPixelAnalysis" : { "enabled" : true,
				 "output"  : "output/hitmap.root" } },
	
	{ "Clustering"   : { "enabled" : false,
			     "output"  : "output/clusters.root" } }
	
    ],
```

This specification should be read as:
> The analysis flow is "HotPixelAnalysis" and "Clustering" in this order. "HotPixelAnalysis" is enabled, while "Clustering" is disabled.

Very straightforward (please note the JSON array is used to specify the order of the analysese).

Then `IAnalysis::play()` calls the constructor of `HotPixelAnalysis`, and do `HotPixelAnalysis::processAll()`.

`processAll()` is inherited from the abstract base class `IAnalysis`:
```c++
void IAnalysis::processAll() {
  init();
  loop();
  end();
}
```
so there are 3 steps as always: initialisation, event loop, and finalisation.

`IAnalysis::loop()` is also defined as a common loop for `TTree`, and only `IAnalysis::processEntry()` inside it is overridden for each concrete analysis class.
```c++
void IAnalysis::loop() {
  const Long64_t nEntries = tree->GetEntries();
  for(Long64_t ientry = 0; ientry< nEntries; ientry++) {
    tree->GetEntry(ientry);
    processEntry();
  }
}
```

# RawAnalysis

## HotPixelAnalysis
### Pre-requisites:
* The input raw ntuple is available.

### Description
This analysis does the following processes:
* creates occupancy map for each sensor
* identifies noisy pixels
* store the noise mask map in the output file

**ToDo**: paramterisze the noise discrimination threshold in user's config.

## Clustering
### Pre-requisites:
* The input raw ntuple is available.
* The `HotPixelAnalysis` output needs to be provided as the condition data.

This analysis use the mask map of the HotPixelAnalysis as the input condition.

### Description
The analysis does clustering. The adjointing pixel hits are grouped, 
and the center of the gravity of the ToT is calculated. 
Up to this stage, coordinate systems are not introduced.

The output ntuple contains the following cluster informations for each event.
* plane (`vector<int>`)
* size (`vector<int>`)
* ave_col (`vector<double>`)
* ave_row (`vector<double>`)
* sumToT (`vector<double>`)

**ToDo**: put options to add more variables, e.g. `size_col`, `size_row`, `sumCharge`.

# ClusterAnalysis

## MaskMap
### Pre-requisites:
* The output of clustering is available as input.

### Description
This analysis attempts to further reduce noise hits at the cluster level.
The strategy is very similar to HotPixelAnalysis.

A noise mask map for `ClusterAnalysis` is created as the output.

## Correlator

### Pre-requisites:
* The output of clustering is available as input.
* The `MaskMap` output needs to be provided as the condition data.

### Description
This analysis checks the validity of the geometry specification 
by taking the position correlation of hits between plane 0 and other planes.

* The threshold in the `MaskMap` should be tuned if noisy area are found.
* If the correlation is not visible nor **positive**, your specification of the geometry, in particular the `FlipRot` matrix is likely incorrect.

The $`(x,y)`$ position calculated here is an estimated global position for each sensor without alignment.

Under the condition that the `FlipRot` matrix is correct, 
the analysis calculates the difference of the hit position 
between plane 0 and other planes, and store in the output root file.

## Alignment

### Pre-requisites:
* The output of clustering is available as input.
* Both of `MaskMap` and `Correlator` outputs need to be provided as the condition data.

### Description
This algorithm is the important step to identify the global coordinate of clusters, and perform track reconstruction.

The task is classified in 4 steps:
* Perform pre-alignment using the `Correlator` output (as a part of `init()`)
* Perform track finding and selection (the main event loop)
* Perform alignment iterations and refit tracks (as a part of post-process in `end()`) .
* Perform final track selection and store clusters on tracks in the output (as a part of post-process in `end()`).

These include some careful checks and the reconstruction may require some tweaking of parameters.

Also one would need to think of e.g. hit requirements to be applied to obtain e.g. the hit efficiency in an unbiased way.

## Tracking

### Pre-requisites:
* The output of clustering is available as input.
* Both of `MaskMap` and `Correlator` outputs need to be provided as the condition data.
* The output of the alignment calculation (default: `output/alignment.json`) is available.

### Description
(*To be added*)


# TrackAnalysis

## TrackAnaExample
At this stage the reconstruction is over, and one can enjoy the results of the test beam.
Since the output is a simple flat Ntuple, it is not so important that the framework provides a rigid way of analysis.
This example gives a possible way of analyzing the output hits using this `focaccia`.

# Algorithm detail in Tracking and Alignment

## Pre-alignment
* The peak in the difference distribution with respect to Plane 0 is identified by peak fitting in x and y direction for each plane.

## Track finding
* The seed cluster is the clusters in the first plane (usually the plane 0).
* Move to the second plane. Search clusters which have the global coordinate $`(x_{j}, y_{j})`$ close to the seed cluster's position $`(x_{0},y_{0})`$ <br /> within the specified acceptance radius $`r_{i}`$ for the plane $`{i}`$:

```math
(x_{j}-x_{0})^{2} + (y_{j}-y_{0})^{2} < r_{i}^{2}
```

* If multiple candidate clusters are found, the track is *branched* and the track finding continues for all branched track candidates.
* Continue this procedure up to the last plane.
* Require a specified hit requirement on the planes.

## Track fitting
* The true track is modelled as just a straight line. No multiple scattering nor magnetic bending is considered so far.
* A simple $`\chi^{2}`$-based track fitting is performed by minimizing the residual. `Minuit` is used for minimization.
* By default, only the `telescope` hits are used to determine track parameters.
* The error scaling for each plane will modify this track $`\chi^{2}`$.
* The resolution of each pixel is by default defined as $`\epsilon w_{col,row}/\sqrt{12}`$, where $`w_{col,row}`$ is the pixel widths, the and the error scale $`\epsilon`$ adjust it. <br />(*This model is naive and not very valid for charge/ToT-weighted clustering, at the moment the $`\chi^{2}`$ tend to be evaluated smaller than it should be.*)

## Alignment procedure
* The alignment is performed plane-by-plane, and after each plane's alignment is over, the used tracks are refitted using the updated alignment.
* The alignment degrees of freedom for each cycle is adjustable by configuration.
* The number of cycles is also adjustable by configuration.
* The track $`\chi^{2}`$ requirement as well as the residual cut requirement are adjustable by configuration.
* `Minuit` is used for minimization. Minuit otpions (e.g. `MIGRAD`, `MINIMIZE`) can be specified as an array, <br />and minimization is tried in the specified order until success.
* For each iteration, the alignment $`\chi^{2}`$ for the plane $`i`$ minimized <br />by fixing the track parameters and changing the alignment parameters $`\vec{a}_{i}`$.
* The alignment $`\chi^{2}`$ is defined as the combination of the follows:

  * Translation: $`\chi^{2}_{i,trans}(\vec{a}_{i}) \equiv \sum_{j\in trk} \left[w_{x,i}^{2}|\delta_{x}^{j}|^{2} + w_{y,i}^{2}|\delta_{y}^{j}|^{2}\right]\bigg/ \sum_{j\in trk}2`$ where $`\vec{\delta}_{i}^{j}= \vec{x}_{i,hit}^{j} - \vec{x}_{i,track}^{j}`$ and $`\vec{w}_{i} = 1/\vec{\sigma}_{i}`$.

  * Rotation:  $`\chi^{2}_{i,rot}(\vec{a}_{i}) \equiv \sum_{j\in{trk}}\left|w_{x}w_{y}(\vec{x}_{i,hit}^{j}\times\vec{\delta}_{i}^{j})_{z}\right|^{2}\bigg/\sum_{j\in{trk}}\left[(w_{x,i}x_{i,hit}^{j})^{2}+(w_{y,i}y_{i,hit}^{j})^{2}\right]`$
  
  * Scale: $`\chi^{2}_{i,scale}(\vec{a}_{i}) \equiv \sum_{j\in{trk}}\left[w_{x}x_{i,hit}^{j}\delta_{x,i}^{j}+w_{y}y_{i,hit}^{j}\delta_{y,i}^{j}\right]^{2}\bigg/ \sum_{j\in{trk}}\left[x_{i,hit}^{j}+y_{i,hit}^{j}\right]^{2}`$
* $`\chi^{2}_{i,trans}(\vec{a}_{i})`$ is the standard residual chi-squared, while $`\chi^{2}_{i,rot}(\vec{a}_{i})`$ enhances the residual rotation of the sensor around z-axis. $`\chi^{2}_{i,scale}(\vec{a}_{i})`$ is sensitive to scale adjustment as well as out-of-plane rotations $`R_{x}`$ and $`R_{y}`$.
* At this moment 3 strategies, i.e. `Trans`,  `TransRot`, `TransRotScale` are defined. These are specialisations of `template class PlaneFitter`. In the class `Alignment`, currently `Trans` is hard-coded (*to be improved*).

### Limitation of the alignment
 * The alignment is simply performed plane-by-plane, and global minimization like `Millepede` is not introduced. Several iterations are better to run.
 * There is a possibility that the *telescope weak mode* is introduced. In order to address this, constraints on $`dx/dz`$ and $`dy/dz`$ (see [Track fitting](#track-fitting)) can be introcuded for controling such bases.

