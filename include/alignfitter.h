#ifndef __alignfitter__
#define __alignfitter__

#include "track.h"
#include "geo.h"
#include "Config.h"

#include <thread>
#include <atomic>

#include <TVector3.h>
#include <TGraph2DErrors.h>
#include <Math/Functor.h>
#include <TPolyLine3D.h>
#include <Math/Vector3D.h>
#include <Fit/Fitter.h>

// function Object to be minimized
struct SumDistance2 {
  // the TGraph is a data member of the object
  TGraph2DErrors *fGraph;
  
  SumDistance2(TGraph2DErrors *g) : fGraph(g) {}
  
  // implementation of the function to be minimized
  double operator() (const double *par) {
    assert(fGraph != 0);
    double * x = fGraph->GetX();
    double * y = fGraph->GetY();
    double * z = fGraph->GetZ();
    double * ex = fGraph->GetEX();
    double * ey = fGraph->GetEY();
    //double * ez = fGraph->GetEZ();
    int npoints = fGraph->GetN();
    double sum = 0;
    for (int i  = 0; i < npoints; ++i) {
      TVector3 vi( x[i], y[i], z[i] );
      TVector3 v1( par[0] + par[1]*z[i], par[2]+par[3]*z[i], z[i] );
      double chi2_i = ( (vi.x()-v1.x())*(vi.x()-v1.x())/(ex[i]*ex[i]) + (vi.y()-v1.y())*(vi.y()-v1.y())/(ey[i]*ey[i]) );
      sum += chi2_i;
    }
    return sum;
  }
  
};

namespace Fitter {
  enum strategy {
    kTrans, kTransRot, kTransRotScale
  };
};


//____________________________________________________________________________________________________
template<unsigned int T = Fitter::strategy::kTrans>
class PlaneFitter {
private:
  std::vector< std::shared_ptr<track> >& tracks;
  const unsigned pl;
  const double chi2_cut;
  const double res_cut;
  const int nmax;
  
  const config& cfg;
  
  std::vector<double> chi2_trans;
  std::vector<double> chi2_trans_norm;
  std::vector<double> chi2_rot;
  std::vector<double> chi2_rot_norm;
  std::vector<double> chi2_scale;
  std::vector<double> chi2_scale_norm;
  
  const double get_chi2() const;
  
  static void chi2_perTrack( const std::shared_ptr<track>& trk, const double* par, const unsigned index, PlaneFitter<T>* ptr );
  
public:
  PlaneFitter( const config& c, std::vector< std::shared_ptr<track> >& _tracks, const unsigned& _pl, double _cut, double _res_cut, const int& max=-1  );
  
  // implementation of the function to be minimized
  double operator() (const double *par);
  
  static unsigned nhits;
  
};

#include "details/alignfitter_details.h"

#endif
