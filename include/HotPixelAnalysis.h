#ifndef __hotpixelAnalysis__
#define __hotpixelAnalysis__

#include "RawAnalysis.h"
#include "HitMap.h"

class HotPixelAnalysis_impl;

class HotPixelAnalysis final : public RawAnalysis {
private:
  
  void init()         override;
  void processEntry() override;
  void end()          override;
  
public:
  HotPixelAnalysis( const config& c, const std::string& _filename, const unsigned& num );
  ~HotPixelAnalysis();
  
 private:
  std::unique_ptr<HotPixelAnalysis_impl> m_impl;
};

#endif
