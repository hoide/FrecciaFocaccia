#ifndef __TrackAnalysis__
#define __TrackAnalysis__

#include "IAnalysis.h"
#include "TrackData.h"

class TrackAnalysis : public IAnalysis {
protected:
  trackvars vars;
  std::string filename;
  
  void initTree() override;
  
public:
  TrackAnalysis( const config& c, const std::string& name, const unsigned& id, const std::string& filename );
  virtual ~TrackAnalysis();
};

#endif
