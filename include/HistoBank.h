#ifndef __HistoBank__
#define __HistoBank__

#include <string>
#include <map>
#include <memory>

#include <TH1.h>


class HistoBank {
private:
  std::map<std::string, std::shared_ptr<TH1> > bank;

public:

  template<class H1>
  std::shared_ptr<H1> allocate( const char* name, const char* title, const int nx, const double xmin, const double xmax) {
    bank.emplace( name, std::make_shared<H1>( name, title, nx, xmin, xmax ) );
    return std::dynamic_pointer_cast<H1>( bank[name] );
  };
  
  template<class H2>
  std::shared_ptr<H2> allocate( const char* name, const char* title, const int nx, const double xmin, const double xmax, const int ny, const double ymin, const double ymax) {
    bank.emplace( name, std::make_shared<H2>( name, title, nx, xmin, xmax, ny, ymin, ymax ) );
    return std::dynamic_pointer_cast<H2>( bank[name] );
  };
  
  HistoBank() {}
  ~HistoBank(){}
};


#endif
