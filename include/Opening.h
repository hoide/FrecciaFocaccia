#ifndef __Opening__
#define __Opening__

#include <iostream>

void opening(const std::string& version, const bool batch) noexcept {
  
  if( !batch ) {
    std::cout << "===========================================================" << std::endl;
    std::cout << "                                                           " << std::endl;
    std::cout << "    /|   /|   /|   /|      /|       /|   /|   /|   /|      " << std::endl;
    std::cout << "   | |  | |  | |  | |     | |      | |  | |  | |  | |      " << std::endl;
    std::cout << "\x1b[32m---\x1b[0m|F\x1b[32m---\x1b[0m|o\x1b[32m---\x1b[0m|c\x1b[32m---\x1b[0m|a\x1b[32m"
      "------\x1b[0m|?\x1b[32m-------\x1b[0m|c\x1b[32m---\x1b[0m|i\x1b[32m---\x1b[0m|a\x1b[32m---\x1b[0m|!\x1b[32m--->>> \x1b[0m" << std::endl;
    std::cout << "   | |  | |  | |  | |     | |      | |  | |  | |  | |      " << std::endl;
    std::cout << "   |/   |/   |/   |/      |/       |/   |/   |/   |/       " << std::endl;
    std::cout << "                                                           " << std::endl;
    std::cout << "                 Freccia Focaccia (v" << version << ")                   " << std::endl;
    std::cout << "                                                           " << std::endl;
    std::cout << "  -- A light-weight testbeam analysis tool from Genova --  " << std::endl;
    std::cout << "                                                           " << std::endl;
    std::cout << "                 provided by Hide Oide                     " << std::endl;
    std::cout << "                (Hideyuki.Oide@cern.ch)                    " << std::endl;
    std::cout << "                                                           " << std::endl;
    std::cout << "===========================================================" << std::endl;
  } else {
    std::cout << "===========================================================" << std::endl;
    std::cout << "                 Freccia Focaccia (v" << version << ")                   " << std::endl;
    std::cout << "===========================================================" << std::endl;
  }
    
}

void ending(const double& elapsed) noexcept {
  std::cout << "\n===========================================================\n" << std::endl;
  std::cout << Form("All processes done! %.2f sec in total", elapsed*1.e-3 ) << std::endl;
  std::cout << "\n===========================================================\n\n" << std::endl;
}

#endif
