#ifndef __AlignMonitoring__
#define __AlignMonitoring__

#include <memory>
#include <map>
#include <vector>

class TFile;
class TH1;
class TH1F;
class TH2F;
class TProfile;
class TProfile2D;
class TF1;
class TF2;

typedef struct AlignMonitoring {
  
  using h1_holder_raw = std::map<int, TH1F* >;
  using h2_holder_raw = std::map<int, TH2F* >;
  using h1_holder     = std::map<int, std::shared_ptr<TH1F> >;
  using h2_holder     = std::map<int, std::shared_ptr<TH2F> >;
  using p1_holder     = std::map<int, std::shared_ptr<TProfile> >;
  using p2_holder     = std::map<int, std::shared_ptr<TProfile2D> >;
  using f1_holder     = std::map<int, std::shared_ptr<TF1> >;
  using f2_holder     = std::map<int, std::shared_ptr<TF2> >;

  using h1_ptr        = std::shared_ptr<TH1F>;
  using h2_ptr        = std::shared_ptr<TH2F>;
  
  std::vector<std::shared_ptr<TH1> > histbank;

  h1_holder_raw xxdiffs;
  h1_holder_raw yydiffs;
  h1_holder xxdiffs_prealigned;
  h1_holder yydiffs_prealigned;
  h1_holder resx;
  h1_holder resy;
  h1_holder resx_aligned;
  h1_holder resy_aligned;
  h1_holder resx_post;
  h1_holder resy_post;
  p2_holder resxmap;
  p2_holder resymap;
  p2_holder resxmap_post;
  p2_holder resymap_post;
  p1_holder resx_col;
  p1_holder resx_row;
  p1_holder resy_col;
  p1_holder resy_row;
  p1_holder resx_col_post;
  p1_holder resx_row_post;
  p1_holder resy_col_post;
  p1_holder resy_row_post;
  p1_holder resx_x_post;
  p1_holder resx_y_post;
  p1_holder resy_x_post;
  p1_holder resy_y_post;
  f1_holder xxfitter;
  f1_holder yyfitter;
  f2_holder resxmap_fit;
  f2_holder resymap_fit;

  p1_holder fine_localX;
  p1_holder fine_localY;
  
  h1_holder pullx_aligned;
  h1_holder pully_aligned;
  h1_ptr pullx_byPlanes;
  h1_ptr pully_byPlanes;

  h1_ptr track_dxdz_dist;
  h1_ptr track_dydz_dist;
  h1_ptr track_chi2_dist_pre;
  h1_ptr track_chi2_dist_post;
  h1_ptr track_chi2_dist_log_pre;
  h1_ptr track_chi2_dist_log_post;

  h1_ptr align_Tx;
  h1_ptr align_Ty;
  h1_ptr align_Rx;
  h1_ptr align_Ry;
  h1_ptr align_Rz;
  h1_ptr align_Sx;
  h1_ptr align_Sy;
  h1_ptr error_scale_x;
  h1_ptr error_scale_y;
  
  template<class H1>
  std::shared_ptr<H1> allocate( const char* name, const char* title, const int nx, const double xmin, const double xmax) {
    histbank.emplace_back( std::make_shared<H1>( name, title, nx, xmin, xmax ) );
    return std::dynamic_pointer_cast<H1>( histbank.back() );
  };
  
  template<class H2>
  std::shared_ptr<H2> allocate( const char* name, const char* title, const int nx, const double xmin, const double xmax, const int ny, const double ymin, const double ymax) {
    histbank.emplace_back( std::make_shared<H2>( name, title, nx, xmin, xmax, ny, ymin, ymax ) );
    return std::dynamic_pointer_cast<H2>( histbank.back() );
  };
  
  template<class F1>
  std::shared_ptr<F1> allocate( const char* name, const char* title, const double xmin, const double xmax) {
    return std::make_shared<F1>( name, title, xmin, xmax );
  };
  
  template<class F2>
  std::shared_ptr<F2> allocate( const char* name, const char* title, const double xmin, const double xmax, const double ymin, const double ymax) {
    return std::make_shared<F2>( name, title, xmin, xmax );
  };

  void initMonitoring();
  
} AlignMonitoring;


#endif
