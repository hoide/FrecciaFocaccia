#ifndef __CLUSTERRATA__
#define __CLUSTERRATA__

#include "rawdata.h"
#include <vector>
#include <TFile.h>

class cluster {
public:
  cluster();
  cluster(const cluster&);
  ~cluster();
  cluster& operator=( const cluster& c );
  std::vector<hit>* hits;
  unsigned plane;
  double sumToT;
  unsigned size;
  double ave_col;
  double ave_row;

  void print() const;
};

class clusterdata {
public:
  std::vector<int>* plane;
  std::vector<int>* size;
  std::vector<double>* ave_col;
  std::vector<double>* ave_row;
  std::vector<double>* sumToT;
  clusterdata();
  ~clusterdata();
  const unsigned num_clus() const { return plane->size(); }
  const cluster get( const unsigned iclus ) const {
    cluster c;
    c.plane  = plane->at( iclus );
    c.sumToT = sumToT->at( iclus );
    c.ave_col = ave_col->at( iclus );
    c.ave_row = ave_row->at( iclus );
    c.size    = size->at( iclus );
    return c;
  }
private:
};

#endif
