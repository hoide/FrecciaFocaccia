#include "AlignMonitoring.h"
#include "geo.h"

#include <TFile.h>
#include <TH1F.h>
#include <TF2.h>
#include <TProfile.h>
#include <TProfile2D.h>

void AlignMonitoring::initMonitoring() {
  
  for( const auto& plane: geo::planes ) {
    if( plane != 0 ) {
      xxdiffs_prealigned.emplace(plane, std::shared_ptr<TH1F>( dynamic_cast<TH1F*>( xxdiffs[plane]->Clone( Form("xxdiff_prealigned_pl0_pl%u", plane) ) ) ) );
      yydiffs_prealigned.emplace(plane, std::shared_ptr<TH1F>( dynamic_cast<TH1F*>( yydiffs[plane]->Clone( Form("yydiff_prealigned_pl0_pl%u", plane) ) ) ) );
      
      xxdiffs_prealigned[plane]->Reset();
      yydiffs_prealigned[plane]->Reset();
    }
    
    resx          .emplace( plane, allocate<TH1F>       ( Form("residual_x_pl%u",plane), "Residual X [mm]", 4000, -2.0, 2.0 ) );
    resy          .emplace( plane, allocate<TH1F>       ( Form("residual_y_pl%u",plane), "Residual Y [mm]", 4000, -2.0, 2.0 ) );
    
    resx_aligned  .emplace( plane, allocate<TH1F>       ( Form("residual_aligned_x_pl%u",plane), "Residual X [mm]", 4000, -2.0, 2.0 ) );
    resy_aligned  .emplace( plane, allocate<TH1F>       ( Form("residual_aligned_y_pl%u",plane), "Residual Y [mm]", 4000, -2.0, 2.0 ) );
    
    pullx_aligned .emplace( plane, allocate<TH1F>       ( Form("pull_aligned_x_pl%u",plane), "Pull X", 200, -5.0, 5.0 ) );
    pully_aligned .emplace( plane, allocate<TH1F>       ( Form("pull_aligned_y_pl%u",plane), "Pull Y", 200, -5.0, 5.0 ) );
    
    resx_post     .emplace( plane, allocate<TH1F>       ( Form("residual_post_x_pl%u",plane), "Residual X [mm]", 4000, -2.0, 2.0 ) );
    resy_post     .emplace( plane, allocate<TH1F>       ( Form("residual_post_y_pl%u",plane), "Residual Y [mm]", 4000, -2.0, 2.0 ) );
    
    const auto& ncol = geo::geometry[plane].ncol();
    const auto& nrow = geo::geometry[plane].nrow();
    
    resxmap       .emplace( plane, allocate<TProfile2D> ( Form("residual_x_map_pl%u",plane), ";Cluster Col;Cluster Row;Residual X [mm]", ncol/5, -0.5, ncol-0.5, nrow/16, -0.5, nrow-0.5 ) );
    resymap       .emplace( plane, allocate<TProfile2D> ( Form("residual_y_map_pl%u",plane), ";Cluster Col;Cluster Row;Residual Y [mm]", ncol/5, -0.5, ncol-0.5, nrow/16, -0.5, nrow-0.5 ) );
    
    resx_col      .emplace( plane, allocate<TProfile>   ( Form("residual_x_col_pl%u",plane), ";Cluster Col;Residual X [mm]", ncol, -0.5, ncol-0.5 ) );
    resx_row      .emplace( plane, allocate<TProfile>   ( Form("residual_x_row_pl%u",plane), ";Cluster Row;Residual X [mm]", nrow, -0.5, nrow-0.5 ) );
    
    resy_col      .emplace( plane, allocate<TProfile>   ( Form("residual_y_col_pl%u",plane), ";Cluster Col;Residual Y [mm]", ncol, -0.5, ncol-0.5 ) );
    resy_row      .emplace( plane, allocate<TProfile>   ( Form("residual_y_row_pl%u",plane), ";Cluster Row;Residual Y [mm]", nrow, -0.5, nrow-0.5 ) );
    
    resxmap_fit   .emplace( plane, allocate<TF2>        ( Form("residual_x_map_fit_pl%u",plane), "[0]+[1]*x+[2]*y", 0.0, ncol*1.0, 0.0, nrow*1.0) );
    resymap_fit   .emplace( plane, allocate<TF2>        ( Form("residual_y_map_fit_pl%u",plane), "[0]+[1]*x+[2]*y", 0.0, ncol*1.0, 0.0, nrow*1.0) );
    
    resxmap_post  .emplace( plane, allocate<TProfile2D> ( Form("residual_post_x_map_pl%u",plane), ";Cluster Col;Cluster Row;Residual X [mm]", ncol/5, -0.5, ncol-0.5, nrow/16, -0.5, nrow-0.5 ) );
    resymap_post  .emplace( plane, allocate<TProfile2D> ( Form("residual_post_y_map_pl%u",plane), ";Cluster Col;Cluster Row;Residual Y [mm]", ncol/5, -0.5, ncol-0.5, nrow/16, -0.5, nrow-0.5 ) );
    
    resx_col_post .emplace( plane, allocate<TProfile>   ( Form("residual_post_x_col_pl%u",plane), ";Cluster Col;Residual X [mm]", ncol, -0.5, ncol-0.5 ) );
    resx_row_post .emplace( plane, allocate<TProfile>   ( Form("residual_post_x_row_pl%u",plane), ";Cluster Row;Residual X [mm]", nrow, -0.5, nrow-0.5 ) );
    
    resy_col_post .emplace( plane, allocate<TProfile>   ( Form("residual_post_y_col_pl%u",plane), ";Cluster Col;Residual Y [mm]", ncol, -0.5, ncol-0.5 ) );
    resy_row_post .emplace( plane, allocate<TProfile>   ( Form("residual_post_y_row_pl%u",plane), ";Cluster Row;Residual Y [mm]", nrow, -0.5, nrow-0.5 ) );
    
    resx_x_post   .emplace( plane, allocate<TProfile>   ( Form("residual_post_x_x_pl%u",plane), ";Track X [mm];Residual X [mm]", 800, -20, 20 ) );
    resx_y_post   .emplace( plane, allocate<TProfile>   ( Form("residual_post_x_y_pl%u",plane), ";Track Y [mm];Residual X [mm]", 800, -20, 20 ) );
    
    resy_x_post   .emplace( plane, allocate<TProfile>   ( Form("residual_post_y_x_pl%u",plane), ";Track X [mm];Residual Y [mm]", 800, -20, 20 ) );
    resy_y_post   .emplace( plane, allocate<TProfile>   ( Form("residual_post_y_y_pl%u",plane), ";Track Y [mm];Residual Y [mm]", 800, -20, 20 ) );
    
    fine_localX   .emplace( plane, allocate<TProfile>(Form("prof_fineX_pl%u", plane), ";Cluster Average Column;localX(track) - localX(cluster) [mm]", ncol, -0.5, ncol-0.5 ) );
    fine_localY   .emplace( plane, allocate<TProfile>(Form("prof_fineY_pl%u", plane), ";Cluster Average Row;localY(track) - localY(cluster) [mm]",    nrow, -0.5, nrow-0.5 ) );
    
  }
  
  pullx_byPlanes           = allocate<TH1F>("pullx", ";Plane;Pull X",  geo::planes.size(), 0, geo::planes.size() );
  pully_byPlanes           = allocate<TH1F>("pully", ";Plane;Pull Y",  geo::planes.size(), 0, geo::planes.size() );
  
  align_Tx                 = allocate<TH1F>("align_Tx", ";Plane;Tx [mm]",  geo::planes.size(), 0, geo::planes.size() );
  align_Ty                 = allocate<TH1F>("align_Ty", ";Plane;Ty [mm]",  geo::planes.size(), 0, geo::planes.size() );
  align_Rx                 = allocate<TH1F>("align_Rx", ";Plane;Rx [rad]", geo::planes.size(), 0, geo::planes.size() );
  align_Ry                 = allocate<TH1F>("align_Ry", ";Plane;Ry [rad]", geo::planes.size(), 0, geo::planes.size() );
  align_Rz                 = allocate<TH1F>("align_Rz", ";Plane;Rz [rad]", geo::planes.size(), 0, geo::planes.size() );
  align_Sx                 = allocate<TH1F>("align_Sx", ";Plane;Sx",       geo::planes.size(), 0, geo::planes.size() );
  align_Sy                 = allocate<TH1F>("align_Sy", ";Plane;Sy",       geo::planes.size(), 0, geo::planes.size() );
  
  error_scale_x            = allocate<TH1F>("error_scale_x", ";Plane;Error Scale X", geo::planes.size(), 0, geo::planes.size() );
  error_scale_y            = allocate<TH1F>("error_scale_y", ";Plane;Error Scale Y", geo::planes.size(), 0, geo::planes.size() );
  
  track_dxdz_dist          = allocate<TH1F>("track_dxdz_dist_pre",  ";dxdz;", 1000, -1.e-2, 1.e-2);
  track_dydz_dist          = allocate<TH1F>("track_dydz_dist_pre",  ";dxdz;", 1000, -1.e-2, 1.e-2);
  
  track_chi2_dist_pre      = allocate<TH1F>("track_chi2_dist_pre",  ";#chi^{2}/N_{dof};", 1000, 0, 100);
  track_chi2_dist_post     = allocate<TH1F>("track_chi2_dist_post", ";#chi^{2}/N_{dof};", 1000, 0, 100);
  
  track_chi2_dist_log_pre  = allocate<TH1F>("track_chi2_dist_log_pre",  ";log_{10}(#chi^{2}/N_{dof});", 140, -2.0, 5.0);
  track_chi2_dist_log_post = allocate<TH1F>("track_chi2_dist_log_post", ";log_{10}(#chi^{2}/N_{dof});", 140, -2.0, 5.0);
  
}
