#include "track.h"
#include "geo.h"
#include "IAnalysis.h"

#include <iostream>

measurement::measurement( const unsigned& _plane, TVector3 _hit )
  : plane( _plane ), hit( _hit )
{
  residual.SetXYZ( 99999, 99999, 99999 );
}

measurement* measurement::clone() const {
  measurement *m = new measurement( this->plane, this->hit );
  m->clus = this->clus;
  return m;
};


void measurement::print() const {
  std::cout << "  -> meas " << this << ": plane " << plane
            << Form(": pos = (%.3f, %.3f, %.3f)", hit.x(), hit.y(), hit.z() ) << std::endl;
  (*clus).print();
}



track::track() : status( active ) {  }

track::track( const track& t ) {
  for( auto& meas : t.measurements ) {
    this->measurements.emplace_back( meas );
  }
  this->x0     = t.x0;
  this->y0     = t.y0;
  this->dxdz   = t.dxdz;
  this->dydz   = t.dydz;
  this->chi2   = t.chi2;
  this->status = t.status;
}

track::~track() {
  //std::cout << "track::dtor done." << std::endl;
}


track* track::clone() const {

  // not a deep copy.
  // Each measurement is std::shared_ptr<measurement>.
  
  track *trk = new track;
  for( const auto &m : this->measurements ) {
    trk->measurements.emplace_back( m );
  }
  trk->x0     = this->x0;
  trk->y0     = this->y0;
  trk->dxdz   = this->dxdz;
  trk->dydz   = this->dydz;
  trk->chi2   = this->chi2;
  trk->status = active;
  return trk;
}

void track::print() const {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::cout << this << ": meas_size = " << measurements.size() << std::endl;
  std::cout << Form("x0 = %.3f, y0 = %.3f, dxdz = %.3e, dydz = %.3e, chi2 = %.3e", x0, y0, dxdz, dydz, chi2) << std::endl;
  for( const auto& m : measurements ) {
    (*m).print();
  }
}


bool track::hasHitAt( const unsigned& plane ) const {
  for( const auto& meas : measurements ) {
    if( (*meas).plane == plane ) return true;
  }
  return false;
}

unsigned track::numberOfHitsAt( const unsigned& plane ) const {
  unsigned num = 0;
  for( const auto& meas : measurements ) {
    if( (*meas).plane == plane ) num++;
  }
  return num;
}

void track::calc_residuals() {
  for( auto& meas : measurements ) {
    
    const auto& plane = (*meas).plane;
    
    TVector3 hit = (*meas).hit;
    
    geo::geometry[plane].apply_alignment( hit );
    
    const double& z = hit.z();
    TVector3 trk_pos = get_global( plane );
    (*meas).residual = hit - trk_pos;
  }
}


TVector3 track::get_global( const unsigned& plane ) const {
  const auto& geom = geo::geometry[plane];
  
  // Alignment parameters to write them short.
  const double& Tx  = geom.Trans.x();
  const double& Ty  = geom.Trans.y();
  const double& Tz  = geom.Trans.z();
  const double& Rx  = geom.Rot.x();  const double& cx = cos(Rx); const double& sx = sin(Rx);
  const double& Ry  = geom.Rot.y();  const double& cy = cos(Ry); const double& sy = sin(Ry);
  const double& Rz  = geom.Rot.z();  const double& cz = cos(Rz); const double& sz = sin(Rz);
  const double& Sx  = geom.Scale.X();
  const double& Sy  = geom.Scale.Y();
  
  // The matrix S[i,j] is a 3x3 matrix equivalent to successive operations of Rx -> Ry -> Rz.
  const double s11 =  cz*cy;
  const double s12 =  cz*sy*sx - sz*cx;
  const double s13 =  cz*sy*cx - sz*sx;
  const double s21 =  sz*cy;
  const double s22 =  sz*sy*sx + cz*cx;
  const double s23 =  sz*sy*cx - cz*sx;
  const double s31 = -sy;
  const double s32 =  cy*sx;
  const double s33 =  cy*cx;
  
  const double detS = s11*s22 - s12*s21;
  
  
  // (u, v, w) are coefficients to map global (x, y) to local (x, y)
  // x_local = ux * x_global + vx * y_global + wx
  // y_local = uy * x_global + vy * y_global + wy
  const double ux =  s22/detS;
  const double vx = -s12/detS;
  const double wx = (-s22*Tx + s12*Ty)/detS;
  
  const double uy = -s21/detS;
  const double vy =  s11/detS;
  const double wy = (s21*Tx - s11*Ty)/detS;
  
  // The matrix A[i, j] and the vector b[i] forms a linear equation
  // A * r = b
  // where r = r(x_global, y_global) is the solution
  
  const double a11 = -dxdz*sy*ux + dxdz*cy*sx*uy - 1.0;
  const double a12 = -dxdz*sy*vx + dxdz*cy*sx*vy;
  const double a21 = -dydz*sy*ux + dydz*cy*sx*uy;
  const double a22 = -dydz*sy*vx + dydz*cy*sx*vy - 1.0;
  
  const double detA = a11*a22 - a12*a21;
  
  const double b1 = dxdz*sy*wx - dxdz*cy*sx*wy - x0 - dxdz * Tz;
  const double b2 = dydz*sy*wx - dydz*cy*sx*wy - y0 - dydz * Tz;
  
  // Solve the linear equation
  const double x = ( a22*b1 - a12*b2)/detA;
  const double y = (-a21*b1 + a22*b2)/detA;
  
  // xx and yy are local coordinates
  const double xx = ( s22*(x-Tx) - s12*(y-Ty) )/detS / (1.0+Sx);
  const double yy = (-s21*(x-Tx) + s11*(y-Ty) )/detS / (1.0+Sy);
  
  // z position is on the sensor plane
  const double z = -sy * xx + (cy*sx) * yy + Tz;
  
  if( IAnalysis::verbosity >= IAnalysis::verbose ) {
    std::cout << Form("S = [ %.3f, %.3f, %.3f, %.3f ]", s11, s12, s21, s22) << ", detS = " << detS << std::endl;
    std::cout << Form("(u, v, w)_x = [%.3f, %.3f, %.3f]", ux, vx, wx ) << std::endl;
    std::cout << Form("(u, v, w)_y = [%.3f, %.3f, %.3f]", uy, vy, wy ) << std::endl;
    std::cout << Form("A = [ %.3f, %.3f, %.3f, %.3f ]", a11, a12, a21, a22) << ", detA = " << detA << std::endl;
    std::cout << Form("b = (%.3f, %.3f)", b1, b2) << std::endl;
    std::cout << Form("(x0,y0) = (%.3f, %.3f)", x0, y0) << std::endl;
    std::cout << Form("(x, y ) = (%.3f, %.3f)", x,  y ) << std::endl;
    std::cout << Form("(xx,yy) = (%.3f, %.3f)", xx, yy) << std::endl;
  }
  
  return TVector3( x, y, z );
}

