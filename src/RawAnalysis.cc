#include "RawAnalysis.h"

#include <iostream>

RawAnalysis::RawAnalysis( const config& c, const std::string& name, const unsigned& num, const std::string& fname )
  : IAnalysis( c, name, "RawAnalysis", num )
  , filename( fname )
{}

RawAnalysis::~RawAnalysis(){
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void RawAnalysis::initTree() {
  file = std::unique_ptr<TFile>( TFile::Open( filename.c_str() ) );
  if( !file->IsOpen() ) throw( "File is not open!" );
  
  tree = dynamic_cast<TTree*>( file->Get("tree") );
  if( !tree ) throw( "Tree is not available. Maybe wrong input file specified?" );

  tree->SetBranchAddress("euEvt", &(data.euEvt) );
  tree->SetBranchAddress("iden", &(data.iden) );
  tree->SetBranchAddress("row", &(data.row) );
  tree->SetBranchAddress("col", &(data.col) );
  tree->SetBranchAddress("tot", &(data.tot) );
  tree->SetBranchAddress("lv1", &(data.lv1) );
}

