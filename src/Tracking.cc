#include "Tracking.h"
#include "Sensor.h"
#include "geo.h"

#include <TH1F.h>
#include <TF1.h>
#include <TF2.h>
#include <TProfile.h>
#include <TProfile2D.h>

#include <fstream>
#include <algorithm>

//____________________________________________________________________________________________________
Tracking::Tracking( const config& c, const std::string& _filename, const unsigned& num )
  : ClusterAnalysis( c, "Tracking", _filename, num )
  , maskmap_file( nullptr )
{
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}


//____________________________________________________________________________________________________
Tracking::Tracking( const config& c, const std::string& _ananame, const std::string& _filename, const unsigned& num )
  : ClusterAnalysis( c, _ananame, _filename, num )
  , maskmap_file( nullptr )
{
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}


//____________________________________________________________________________________________________
Tracking::~Tracking() {
  //for( auto *trk : *track_accum ) delete trk;
  //delete track_accum;
}


//____________________________________________________________________________________________________
void Tracking::init() {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  initTree();
  loadInputs();
  initMonitoring();
  
  geo::load_alignment( cfg );
  
  std::cout << "\n----------------------------" << std::endl;
  
}

//____________________________________________________________________________________________________
void Tracking::loadInputs() {
  
  const std::string maskfilename = cfg[ana_type][0]["MaskMap"]["output"];
  
  maskmap_file = TFile::Open( maskfilename.c_str() );
  for( const auto& plane: geo::planes ) {
    maskmaps[plane] = dynamic_cast<TH2F*>( maskmap_file->Get( Form("xymap_pl%u_mask", plane) ) );
    if( !maskmaps[plane] ) {
      throw( Form("ERROR! maskmap plane %u is null!", plane ) );
    }
  }
  
  const std::string correlator_filename = cfg[ana_type][1]["Correlator"]["output"];
  
  TFile* correlator_file = TFile::Open( correlator_filename.c_str() );
  
  for( const auto& plane: geo::planes ) {
    if( plane == geo::origin_plane ) continue;
    xxdiffs.emplace(plane, dynamic_cast<TH1F*>( correlator_file->Get( Form("xxdiff_pl0_pl%u", plane) ) ) );
    yydiffs.emplace(plane, dynamic_cast<TH1F*>( correlator_file->Get( Form("yydiff_pl0_pl%u", plane) ) ) );
    if( !( xxdiffs[plane] and yydiffs[plane] ) ) {
      throw( Form("ERROR! correlator diff plane %u is null!", plane ) );
    }
    
    auto xxhist = xxdiffs[plane];
    const double max_x = xxhist->GetBinCenter( xxhist->GetMaximumBin() );
    xxfitter.emplace( plane, allocate<TF1>(Form("fit_xxdiff_pl%u", plane), "gaus+pol0(3)", max_x - 0.5, max_x+0.5 ) );
    xxfitter[plane]->SetParameter(0, xxhist->GetMaximum() );
    xxfitter[plane]->SetParameter(1, max_x );
    xxfitter[plane]->SetParameter(2, 0.05 );
    xxhist->Fit( xxfitter[plane]->GetName(), "rq+" );
    
    auto yyhist = yydiffs[plane];
    const double max_y = yyhist->GetBinCenter( yyhist->GetMaximumBin() );
    yyfitter.emplace( plane, allocate<TF1>(Form("fit_yydiff_pl%u", plane), "gaus+pol0(3)", max_y - 0.5, max_y+0.5 ) );
    yyfitter[plane]->SetParameter(0, yyhist->GetMaximum() );
    yyfitter[plane]->SetParameter(1, max_y );
    yyfitter[plane]->SetParameter(2, 0.05 );
    yyhist->Fit( yyfitter[plane]->GetName(), "rq+" );
    
    std::cout << Form("plane %2u  prealign = (%6.3f, %6.3f) mm", plane, xxfitter[plane]->GetParameter(1), yyfitter[plane]->GetParameter(1) )
              << std::endl;
    geo::geometry[plane].pre_align.Set( xxfitter[plane]->GetParameter(1), yyfitter[plane]->GetParameter(1) );
    
  }
  
  std::cout << std::endl;
  
}


//____________________________________________________________________________________________________
bool Tracking::is_masked( const cluster& c ) {
  
  bool flag = false;
  TVector3 pos = get_global_pos( c, geo::prealign::wo_prealign );
  
  if( maskmaps[c.plane]
      ->GetBinContent( maskmaps[c.plane]->GetXaxis()->FindBin( pos.X() ),
                       maskmaps[c.plane]->GetXaxis()->FindBin( pos.Y() ) ) > 0 ) {
    flag = true;
  }
  return flag;
}


//____________________________________________________________________________________________________
void Tracking::get_measurements( measurement_bank& hits ) {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  for( unsigned iclus=0; iclus<data.num_clus(); iclus++) {
    const auto& clus = data.get( iclus );
    
    if( is_masked( clus ) ) continue;
    
    TVector3 pos = get_global_pos( clus, geo::prealign::with_prealign, geo::align::wo_align );
    if( verbosity >=verbose ) std::cout << Form(" --> (%.2f, %.2f)", pos.X(), pos.Y() ) << std::endl;
    
    std::shared_ptr<measurement> meas = std::make_shared<measurement>( clus.plane, pos );
    if( verbosity >= verbose ) clus.print();
    (*meas).clus = std::make_shared<cluster>( clus );
    if( verbosity ) (*meas).print();
    
    hits[clus.plane].emplace_back( meas );
    
  }
  
  // correlator output
  for( unsigned iclus=0; iclus<data.num_clus(); iclus++) {
    const auto& icluster = data.get( iclus );
    if( icluster.plane != geo::origin_plane ) continue;
    
    if( is_masked( icluster ) ) continue;
    
    for( unsigned jclus=0; jclus<data.num_clus(); jclus++) {
      const auto& jcluster = data.get( jclus );
      
      if( is_masked( jcluster ) ) continue;
      
      if( jcluster.plane == geo::origin_plane ) continue;
      
      const TVector3 ipos = get_global_pos( icluster, geo::prealign::with_prealign );
      const TVector3 jpos = get_global_pos( jcluster, geo::prealign::with_prealign );
      
      xxdiffs_prealigned[jcluster.plane]->Fill( jpos.X() - ipos.X() );
      yydiffs_prealigned[jcluster.plane]->Fill( jpos.Y() - ipos.Y() );
      
    }
  }
  
}



//____________________________________________________________________________________________________
void Tracking::find_tracks( measurement_bank& hit_bank ) {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  // Loop over plane0 hits
  std::vector< std::shared_ptr<track> > candidates;
  
  
  const auto& acceptance = cfg["track_finding"]["acceptance"];
  
  std::map<unsigned, double> is_accept;
  for( auto it = acceptance.begin(); it != acceptance.end(); ++it ) {
    auto plane = std::stoi( it.key() );
    auto value = it.value();
    
    if( std::find( geo::planes.begin(), geo::planes.end(), plane ) == geo::planes.end() ) {
      throw( Form("ERROR! Having invalid plane ID in track_finding/acceptance configuration : %u\n", plane) );
    }
    is_accept[plane] = value;
  }
  
  if( is_accept.size() != geo::planes.size() ) {
    std::string missing;
    for( auto& plane : geo::planes ) {
      if( is_accept.find(plane) == is_accept.end() ) {
        missing += Form("%u ", plane);
      }
    }
    throw( Form("ERROR! Missing acceptance specification for planes: %s", missing.c_str()) );
  }
  
  
  auto find_next = [&]( std::pair<unsigned, hit_collection> planehits ) -> void {
    if( verbosity>=verbose ) std::cout << "-----------\n" << __PRETTY_FUNCTION__ << std::endl;
    
    auto& plane = planehits.first;
    auto& measurements  = planehits.second;
    
    if( verbosity>=verbose ) std::cout << "find_next(): plane = " << plane << std::endl;
    
    // loop over hits in the plane
    for( auto& meas : measurements ) {
      
      if( geo::origin_plane == plane ) {
        
        if( verbosity>=verbose ) std::cout << "  -> plane 0: initial meas" << std::endl;
        auto trk = std::make_shared<track>();
        (*trk).measurements.emplace_back( (*meas).clone() );
        candidates.emplace_back( trk );
        
      } else {
        
        // Here, the track loop scan is done only for existing tracks
        for( auto itrk = 0; itrk< candidates.size(); itrk++ ) {
          
          auto trk = candidates.at( itrk );
          
          if( verbosity>=verbose ) std::cout << "  -> track " << trk << std::endl;
          
          if( (*trk).status == track::branched ) {
            if( verbosity>=verbose ) std::cout << "  ->this track is already branched -> skip." << std::endl;
            continue;
          }
          
          if( (*trk).hasHitAt( plane ) ) {
            if( verbosity>=verbose ) std::cout << "  -> the hit is already included -> skip." << std::endl;
            continue;
          }
          
          const auto pos_pl0 = (*trk).measurements[geo::origin_plane]->hit;
          if( ( pos_pl0 - meas->hit ).Perp() < is_accept[plane] ) {
            if( verbosity ) std::cout << Form("  attaching hit (%.2f, %.2f, %.2f)", meas->hit.X(), meas->hit.Y(), meas->hit.Z() ) << std::endl;
            
            auto branched_trk = std::shared_ptr<track>( (*trk).clone() );
            (*branched_trk).measurements.emplace_back( meas->clone() );
            candidates.emplace_back( branched_trk );
            (*trk).status =  track::branched;
            if( verbosity>=verbose ) std::cout << "  -> branched new" << std::endl;
          } else {
            if( verbosity>=verbose ) std::cout << "  -> outlier -> skip" << std::endl;
          }
        }
        
      }
    }
    
    if( verbosity>=verbose ) {
      std::cout << Form("\n---\nEnd of find_next(): #candidates = %lu", candidates.size() ) << std::endl;
      int count_active = 0;
      int count_branched = 0;
      for( auto& trk : candidates ) {
        if( (*trk).status == track::active ) {
          count_active++;
        } else {
          count_branched++;
        }
      }
      std::cout << Form("End of find_next(): #candidates (active) = %d, (branched) = %d", count_active, count_branched ) << std::endl;
    }
    
  };
  
  // Execute track finding
  std::for_each( hit_bank.begin(), hit_bank.end(), find_next );
  
  
  if( verbosity ) std::cout << "----------------\nTrack finding done. #candidates = " << candidates.size() << std::endl;
  
  auto filter = [&]( std::shared_ptr<track>& trk ) ->bool {
    bool flag = false;
    bool multflag = false;
    if( (*trk).status == track::branched ) flag = true;
    
    const auto& hit_req = cfg["track_finding"]["hit_requirement"];
    
    for( const auto& plane : hit_req ) {
      const unsigned& nHits = (*trk).numberOfHitsAt( plane );
      if( 1 != nHits ) flag = true;
      if( nHits > 1 ) multflag = true;
    }

    if( multflag ) (*trk).print();
    
    return flag;
  };
  
  
  // post process.
  // So far all hits in the measurement is pointing to the elements in the measurement_bank.
  // But measurement_bank is lost after this event is over.
  // Therefore, clone each measurement and own them.
  for( auto& trk : candidates ) {
    
    if( verbosity>=verbose ) (*trk).print();
    
    if( !filter( trk ) ) {
      track_accum.emplace_back( trk );
    }
    
  }
  
  candidates.clear();
  
  for( auto plainhits : hit_bank ) {
    plainhits.second.clear();
  }
}


//____________________________________________________________________________________________________
void Tracking::processEntry() {
  if( verbosity ) std::cout << "\n" << __PRETTY_FUNCTION__ << std::endl;

  measurement_bank hits;

  // Convert clusters to hit positions with pre-alignment.
  get_measurements( hits );
  
  // Track finder
  find_tracks( hits );
  
}

//____________________________________________________________________________________________________
void Tracking::end() {
  
  // Track fitting and calculate residuals
  fit_tracks();
  
  // Fill histograms
  for( const auto& trk : track_accum ) {
    if( (*trk).status == track::fit_bad ) continue;
    
    for( const auto& meas : (*trk).measurements ) {
      auto& plane = (*meas).plane;
      resx[plane]->Fill( (*meas).residual.X() );
      resy[plane]->Fill( (*meas).residual.Y() );
    }
    
    track_chi2_dist_pre->Fill( (*trk).chi2 );
    track_chi2_dist_log_pre->Fill( log10((*trk).chi2) );
  }
  
  summary1();
  
  summary2();
  
}




//____________________________________________________________________________________________________
void Tracking::summary1() {
  
  unsigned ntracks_fit = 0;
  unsigned ntracks_good = 0;
  std::map<unsigned, unsigned> nhits_plane;
  for( const auto& plane : geo::planes ) {
    nhits_plane[plane] = 0;
  }
  
  for( auto &trk : track_accum ) {
    if( (*trk).status == track::fit_bad ) continue;
    ntracks_fit++;
    const double& chi2_cut = cfg["alignment"]["chi2_cut"];
    if( (*trk).chi2 > chi2_cut ) continue;
    ntracks_good++;
    for( const auto& meas : (*trk).measurements ) {
      nhits_plane[ (*meas).plane ]++;
    }
  }
  std::cout << "\n----------------------------" << std::endl;
  std::cout << "collected " << ntracks_fit << " tracks." << std::endl;
  std::cout << "collected " << ntracks_good << " good quality tracks." << std::endl;
  for( const auto& plane : geo::planes ) {
    std::cout << Form("plane %2u collected %7u good hits", plane, nhits_plane[plane] ) << std::endl;
  }
  
  
}

//____________________________________________________________________________________________________
void Tracking::summary2() {
  
  fill_resmap();
  
  output();
}


//____________________________________________________________________________________________________
void Tracking::fill_resmap() {
  const double chi2_cut = cfg["tracking"]["chi2_cut"];
  
  std::cout << "Filling residual 2D maps..." << std::endl;
  for( const auto& trk : track_accum ) {
    
    if( (*trk).status == track::fit_bad ) continue;
    if( (*trk).chi2 > chi2_cut )          continue;
    
    for( const auto& meas : (*trk).measurements ) {
      
      if( (*meas).clus->size == 0 ) continue;
      
      const auto& geom = geo::geometry[ (*meas).plane ];
      
      // invert the align correction
      TVector3 global = (*trk).get_global( (*meas).plane );
      
      global.SetXYZ( global.X() + geom.pre_align.X(),
                     global.Y() + geom.pre_align.Y(),
                     global.Z() );
      
      TVector2 local = geom.global_to_local( global );
      
      TVector3 err = geom.global_err();
      
      double _res_x = (*meas).residual.X();
      double _res_y = (*meas).residual.Y();
      
      if( fabs( _res_x ) > 4.0*err.X() ) continue;
      if( fabs( _res_y ) > 4.0*err.Y() ) continue;
      
      resxmap [ (*meas).plane ] -> Fill( (*meas).clus->ave_col, (*meas).clus->ave_row, _res_x );
      resx_col[ (*meas).plane ] -> Fill( (*meas).clus->ave_col, _res_x );
      resx_row[ (*meas).plane ] -> Fill( (*meas).clus->ave_row, _res_x );
      
      resymap [ (*meas).plane ] -> Fill( (*meas).clus->ave_col, (*meas).clus->ave_row, _res_y );
      resy_col[ (*meas).plane ] -> Fill( (*meas).clus->ave_col, _res_y );
      resy_row[ (*meas).plane ] -> Fill( (*meas).clus->ave_row, _res_y );
      
    }
    
  }
  
  
  std::cout << "Residual 2D maps fitting..." << std::endl;
  for( auto& plane : geo::planes ) {
    
    resxmap[ plane ]->Fit( resxmap_fit[ plane ]->GetName(), "rq0" );
    resymap[ plane ]->Fit( resymap_fit[ plane ]->GetName(), "rq0" );
    
  }
  
}


//____________________________________________________________________________________________________
void Tracking::fit_tracks( const int nmax, const int plane ) {
  std::cout << "Fitting tracks..." << std::flush;
  
  track_dxdz_dist->Reset();
  track_dydz_dist->Reset();
  track_chi2_dist_post->Reset();
  track_chi2_dist_log_post->Reset();
  
  for( const auto& pl : geo::planes ) {
    resx_aligned[pl] ->Reset();
    resy_aligned[pl] ->Reset();
    pullx_aligned[pl]->Reset();
    pully_aligned[pl]->Reset();
  }
  
  const double chi2_cut = cfg["tracking"]["chi2_cut"];
  
  
  unsigned ntracks = 0;
  for( auto& trk : track_accum ) {
    fit_track( trk, plane );
    
    if( (*trk).chi2 > chi2_cut ) continue;
    
    track_dxdz_dist->Fill( (*trk).dxdz );
    track_dydz_dist->Fill( (*trk).dydz );
    track_chi2_dist_post->Fill( (*trk).chi2 );
    track_chi2_dist_log_post->Fill( log10((*trk).chi2) );
    
    ntracks++;
    
    for( const auto& meas : (*trk).measurements ) {
      
      const auto& geom = geo::geometry[(*meas).plane];
      const auto& error = geom.global_err();
      
      resx_aligned[(*meas).plane]->Fill( (*meas).residual.X() );
      resy_aligned[(*meas).plane]->Fill( (*meas).residual.Y() );
      pullx_aligned[(*meas).plane]->Fill( (*meas).residual.X()/error.X() );
      pully_aligned[(*meas).plane]->Fill( (*meas).residual.Y()/error.Y() );
      
    }
    
    if( nmax > 0 and ntracks > nmax ) break;
  }
  std::cout << " done." << std::endl;
}


//____________________________________________________________________________________________________
void Tracking::fit_track( std::shared_ptr<track>& trk, const int plane ) {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  TGraph2DErrors * gr = new TGraph2DErrors();
  
  unsigned int used_hits = 0;
  for( auto& meas : (*trk).measurements ) {
    
    const auto& geom = geo::geometry[(*meas).plane];
    
    // Use only telescope hits for determining the track parameters
    if( geom.plane_type == "dut" ) continue;
    
    auto hit = (*meas).hit;
    const auto& err = geom.global_err();
    
    geom.apply_alignment( hit );
    
    gr->SetPoint     ( gr->GetN(), hit.X(), hit.Y(), hit.Z() );
    gr->SetPointError( gr->GetN()-1, err.X(), err.Y(), err.Z() );
    
    used_hits++;
  }
  
  ROOT::Fit::Fitter  fitter;
  
  // make the functor objet
  SumDistance2 sdist(gr);
  ROOT::Math::Functor fcn(sdist,track::ndof);
  
  // set the function and the initial parameter values
  double pStart[track::ndof] = { (*trk).measurements.at(0)->hit.X(), 0, (*trk).measurements.at(0)->hit.Y(), 0 };
  fitter.SetFCN(fcn,pStart);
  
  for (int i = 0; i < track::ndof; ++i) fitter.Config().ParSettings(i).SetStepSize(0.01);
  
  bool ok = fitter.FitFCN();
  
  delete gr;
  
  if( !ok ) {
    (*trk).status = track::fit_bad;
  } else {
    
    const ROOT::Fit::FitResult & result = fitter.Result();
    const double * params = result.GetParams();
    (*trk).x0 = params[0];
    (*trk).y0 = params[2];
    (*trk).dxdz = params[1];
    (*trk).dydz = params[3];
    
    (*trk).calc_residuals();
    
    double chi2 = 0.0;
    for( auto& meas : (*trk).measurements ) {
      
      const double& resx = (*meas).residual.X();
      const double& resy = (*meas).residual.Y();
      const double& errx = geo::geometry[(*meas).plane].global_err().X();
      const double& erry = geo::geometry[(*meas).plane].global_err().Y();
      
      double chi2_i = (resx*resx)/(errx*errx) + (resy*resy)/(erry*erry);
      
      chi2 += chi2_i;
    }
    chi2 /= static_cast<double>(used_hits*2 - track::ndof*1);
    
    (*trk).chi2 = chi2;
  }
  
  return;
  
}

//____________________________________________________________________________________________________
void Tracking::output() {
  
  std::cout << "Writing out..." << std::endl;
  
  std::string outfilename = cfg[ana_type][order][ana_name]["output"];
  
  TFile *ofile = new TFile(outfilename.c_str(), "recreate");
  
  fill_tree();
  
  ofile->Write();
  
  // Other histograms
  ofile->mkdir("residuals");
  gDirectory->cd("residuals");
  
  for( const auto& plane : geo::planes ) {
    gDirectory->mkdir(Form("plane%u", plane));
    gDirectory->cd(Form("plane%u", plane));
    
    resxmap[plane]->SetMinimum(-0.10);
    resxmap[plane]->SetMaximum( 0.10);
    
    resymap[plane]->SetMinimum(-0.10);
    resymap[plane]->SetMaximum( 0.10);
    
    resxmap[plane] ->SetContour(99);
    resymap[plane] ->SetContour(99);
    resxmap_fit[plane] ->SetContour(99);
    resymap_fit[plane] ->SetContour(99);
    
    resx[plane]  ->Write();
    resy[plane]  ->Write();
    resx_aligned[plane] ->Write();
    resy_aligned[plane] ->Write();
    
    resxmap[plane] ->Write();
    resymap[plane] ->Write();
    resxmap_fit[plane] ->Write();
    resymap_fit[plane] ->Write();
    resx_col[plane] ->Write();
    resx_row[plane] ->Write();
    resy_col[plane] ->Write();
    resy_row[plane] ->Write();
    resx_col_post[plane]->Write();
    resx_row_post[plane]->Write();
    resy_col_post[plane]->Write();
    resy_row_post[plane]->Write();
    resx_x_post[plane] ->Write();
    resx_y_post[plane] ->Write();
    resy_x_post[plane] ->Write();
    resy_y_post[plane] ->Write();
    
    fine_localX[plane]->Write();
    fine_localY[plane]->Write();
    
    if( geo::origin_plane != plane ) {
      
      xxdiffs[plane] ->Write();
      yydiffs[plane] ->Write();
      
      xxdiffs_prealigned[plane]->Write();
      yydiffs_prealigned[plane]->Write();
      
    }
    
    ofile->cd();
    gDirectory->cd("residuals");
  }
  
  ofile->cd();
  ofile->mkdir("pulls");
  gDirectory->cd("pulls");
  
  for( const auto& plane : geo::planes ) {
    pullx_aligned[plane] ->Write();
    pully_aligned[plane] ->Write();
  }
  pullx_byPlanes->Write();
  pully_byPlanes->Write();
  
  
  ofile->cd();
  ofile->mkdir("align");
  gDirectory->cd("align");
  
  align_Tx->Write();
  align_Ty->Write();
  align_Rx->Write();
  align_Ry->Write();
  align_Rz->Write();
  align_Sx->Write();
  align_Sy->Write();
  
  error_scale_x->Write();
  error_scale_y->Write();
  
  ofile->cd();
  ofile->mkdir("chi2");
  gDirectory->cd("chi2");
  
  track_chi2_dist_pre ->Write();
  track_chi2_dist_post->Write();
  
  track_chi2_dist_log_pre ->Write();
  track_chi2_dist_log_post->Write();
  
  ofile->cd();
  ofile->mkdir("tracks");
  gDirectory->cd("tracks");
  
  track_dxdz_dist->Write();
  track_dydz_dist->Write();
  
  ofile->Close();
  file->Close();
  
  std::cout << "Tracking Process end." << std::endl;
}



//____________________________________________________________________________________________________
void Tracking::fill_tree() {
  float chi2;
  float x0;
  float y0;
  float dxdz;
  float dydz;
  int    nhits;
  int    nhits_tel;
  std::vector<int> pl;
  std::vector<double> x;
  std::vector<double> y;
  std::vector<double> local_x;
  std::vector<double> local_y;
  std::vector<int> trkcol;
  std::vector<int> trkrow;
  std::vector<double> folded_x;
  std::vector<double> folded_y;
  std::vector<double> res_x;
  std::vector<double> res_y;
  std::vector<int> size;
  std::vector<double> ave_col;
  std::vector<double> ave_row;
  std::vector<double> sumToT;
  
  std::vector<std::vector<int> >    intVectors    { pl, trkcol, trkrow, size };
  std::vector<std::vector<double> > doubleVectors { x, y, local_x, local_y, folded_x, folded_y, res_x, res_y, ave_col, ave_row, sumToT };
  
  TTree *otree = new TTree("trackTree", "trackTree");
  otree->Branch( "chi2", &chi2);
  otree->Branch( "x0", &x0);
  otree->Branch( "y0", &y0);
  otree->Branch( "dxdz", &dxdz);
  otree->Branch( "dydz", &dydz);
  otree->Branch( "nhits", &nhits);
  otree->Branch( "nhits_tel", &nhits_tel);
  otree->Branch( "plane", &pl);
  otree->Branch( "x", &x);
  otree->Branch( "y", &y);
  otree->Branch( "local_x", &local_x);
  otree->Branch( "local_y", &local_y);
  otree->Branch( "trkcol", &trkcol);
  otree->Branch( "trkrow", &trkrow);
  otree->Branch( "folded_x", &folded_x);
  otree->Branch( "folded_y", &folded_y);
  otree->Branch( "ave_col", &ave_col);
  otree->Branch( "ave_row", &ave_row);
  otree->Branch( "size", &size);
  otree->Branch( "sumToT", &sumToT);
  otree->Branch( "res_x", &res_x);
  otree->Branch( "res_y", &res_y);
  
  otree->SetAutoSave();
  
  typedef struct clus_vars {
    int    plane;
    double x;
    double y;
    double local_x;
    double local_y;
    int    trkcol;
    int    trkrow;
    double res_x;
    double res_y;
    double folded_x;
    double folded_y;
    int    size;
    double ave_col;
    double ave_row;
    double sumToT;
  } clus_vars;
  
  clus_vars v;
  
  auto clear = [] (clus_vars& v) {
    v.plane    = -9999;
    v.x        = -9999;
    v.y        = -9999;
    v.local_x  = -9999;
    v.local_y  = -9999;
    v.trkcol   = -9999;
    v.trkrow   = -9999;
    v.res_x    = -9999;
    v.res_y    = -9999;
    v.folded_x = -9999;
    v.folded_y = -9999;
    v.ave_col  = -9999;
    v.size     = 0;
    v.ave_col  = -9999;
    v.ave_row  = -9999;
    v.sumToT   = 0;
  };
  
  
  for( auto& trk : track_accum ) {
    
    if( (*trk).status == track::fit_bad ) continue;
    
    //track selection
    
    const double& chi2_cut = cfg["tracking"]["chi2_cut"];
    if( (*trk).chi2 > chi2_cut ) continue;
    
    nhits_tel = 0;
    
    for( const auto &meas : (*trk).measurements ) {
      if( geo::geometry[(*meas).plane].plane_type != std::string("dut") ) {
        nhits_tel++;
      }
    }
    
    chi2 = (*trk).chi2;
    x0   = (*trk).x0;
    y0   = (*trk).y0;
    dxdz = (*trk).dxdz;
    dydz = (*trk).dydz;
    
    nhits = (*trk).measurements.size();
    
    for( const auto& plane : geo::planes ) {
      
      const auto& geom = geo::geometry[plane];
      
      clear( v );
      
      v.plane = static_cast<int>( plane );
      
      TVector3 global = (*trk).get_global( plane );
      
      TVector2 local  = geom.global_to_local( global );
      
      v.x = global.X();
      v.y = global.Y();
      
      geom.get_pixel( local, v.trkcol, v.trkrow );
      
      v.local_x = local.X();
      v.local_y = local.Y();
      
      TVector2 folded = geom.local_to_folded( local, v.trkcol, v.trkrow, geo::flip::with_flip );
      
      v.folded_x = folded.X();
      v.folded_y = folded.Y();
      
      // Loop over measurements
      for( const auto& meas : (*trk).measurements ) {
        
        // Find the matching plane
        if( (*meas).plane != plane ) continue;
        
        v.size  = static_cast<int>( (*meas).clus->size );
        v.sumToT = (*meas).clus->sumToT;
        
        v.ave_col = (*meas).clus->ave_col;
        v.ave_row = (*meas).clus->ave_row;
        
        v.res_x  = (*meas).residual.X();
        v.res_y  = (*meas).residual.Y();
        
        TVector3 err = geo::geometry[(*meas).plane].global_err();
        
        if( fabs( v.res_x ) > 2.0 * err.X() ) continue;
        if( fabs( v.res_y ) > 2.0 * err.Y() ) continue;
        
        auto trk_pos = (*trk).get_global( plane );
        
        resx_post     [ (*meas).plane ] -> Fill( v.res_x );
        resxmap_post  [ (*meas).plane ] -> Fill( v.ave_col, v.ave_row, v.res_x );
        resx_col_post [ (*meas).plane ] -> Fill( v.ave_col, v.res_x );
        resx_row_post [ (*meas).plane ] -> Fill( v.ave_row, v.res_x );
        resx_x_post   [ (*meas).plane ] -> Fill( global.x(), v.res_x );
        resx_y_post   [ (*meas).plane ] -> Fill( global.y(), v.res_x );
        
        resy_post     [ (*meas).plane ] -> Fill( v.res_y );
        resymap_post  [ (*meas).plane ] -> Fill( v.ave_col, v.ave_row, v.res_y );
        resy_col_post [ (*meas).plane ] -> Fill( v.ave_col, v.res_y );
        resy_row_post [ (*meas).plane ] -> Fill( v.ave_row, v.res_y );
        resy_x_post   [ (*meas).plane ] -> Fill( global.x(), v.res_y );
        resy_y_post   [ (*meas).plane ] -> Fill( global.y(), v.res_y );
        
        fine_localX   [ (*meas).plane ] -> Fill( v.ave_col, v.local_x - (v.ave_col-geom.ncol()/2)*geom.col_width() - geom.col_width()/2.0 );
        fine_localY   [ (*meas).plane ] -> Fill( v.ave_row, v.local_y - (v.ave_row-geom.nrow()/2)*geom.row_width() - geom.row_width()/2.0  );
      }
      
      pl       .emplace_back( v.plane );
      x        .emplace_back( v.x );
      y        .emplace_back( v.y );
      local_x  .emplace_back( v.local_x );
      local_y  .emplace_back( v.local_y );
      trkcol   .emplace_back( v.trkcol );
      trkrow   .emplace_back( v.trkrow );
      folded_x .emplace_back( v.folded_x );
      folded_y .emplace_back( v.folded_y );
      res_x    .emplace_back( v.res_x );
      res_y    .emplace_back( v.res_y );
      ave_col  .emplace_back( v.ave_col );
      ave_row  .emplace_back( v.ave_row );
      size     .emplace_back( v.size );
      sumToT   .emplace_back( v.sumToT );
      
    }
    otree->Fill();
    
    pl       .clear();
    x        .clear();
    y        .clear();
    local_x  .clear();
    local_y  .clear();
    trkcol   .clear();
    trkrow   .clear();
    folded_x .clear();
    folded_y .clear();
    res_x    .clear();
    res_y    .clear();
    ave_col  .clear();
    ave_row  .clear();
    size     .clear();
    sumToT   .clear();
  }
  
}
