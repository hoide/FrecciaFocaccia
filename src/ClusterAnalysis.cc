#include "ClusterAnalysis.h"

#include <iostream>

#include <TFile.h>
#include <TTree.h>
#include <TVector2.h>
#include <TVector3.h>

ClusterAnalysis::ClusterAnalysis( const config& c, const std::string& name, const std::string& fname, const unsigned& id )
  : IAnalysis( c, name, "ClusterAnalysis", id )
  , filename( fname )
{
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}

ClusterAnalysis::~ClusterAnalysis(){
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void ClusterAnalysis::initTree() {

  file = std::unique_ptr<TFile>( TFile::Open( filename.c_str() ) );
  if( !file->IsOpen() ) throw( Form("Input file %s is not open!", filename.c_str() ) );
  tree = dynamic_cast<TTree*>( file->Get("clusterTree") );
  if( !tree ) throw( "Null tree! perhaps you specified a different file?");
  
  tree->SetBranchAddress("plane",   &(data.plane) );
  tree->SetBranchAddress("size",    &(data.size) );
  tree->SetBranchAddress("ave_col", &(data.ave_col) );
  tree->SetBranchAddress("ave_row", &(data.ave_row) );
  tree->SetBranchAddress("sumToT",  &(data.sumToT) );
}


TVector3 ClusterAnalysis::get_global_pos( const cluster& c, geo::prealign do_prealign, geo::align do_align ) {

  return geo::geometry[c.plane].pixel_to_global( c.ave_col, c.ave_row, do_prealign, do_align );

}


TVector2 ClusterAnalysis::get_local_pos( const cluster& c ) {

  return geo::geometry[c.plane].pixel_to_local( c.ave_col, c.ave_row );

}


TVector3 ClusterAnalysis::get_global_err( const cluster& c ) {

  return geo::geometry[c.plane].global_err();
  
}


TVector3 ClusterAnalysis::get_local_err( const cluster& c ) {

  return geo::geometry[c.plane].local_err();
  
}

