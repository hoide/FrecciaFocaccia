#include "MaskMap.h"
#include "geo.h"

#include <iostream>

#include <TVector3.h>
#include <TH2F.h>

MaskMap::MaskMap( const config& c, const std::string& _filename, const unsigned& num )
  : ClusterAnalysis( c, "MaskMap", _filename, num )
{}

MaskMap::~MaskMap() {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void MaskMap::init() {
  
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  initTree();
  
  // telescope hit maps
  for( const auto& plane : geo::planes ) {
    auto *m = new TH2F(Form("xymap_pl%d", plane), ";x [mm];y [mm]", 400, -20, 20, 400, -20, 20 );
    xymaps[plane] = m ;
  }
  
}

void MaskMap::processEntry() {
  
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  for( unsigned iclus=0; iclus<data.num_clus(); iclus++) {
    const auto& cluster = data.get( iclus );
    
    const TVector3 pos = get_global_pos( cluster, geo::prealign::wo_prealign );
    
    xymaps[ cluster.plane ]->Fill( pos.X(), pos.Y() );
  }
  
}

void MaskMap::end() {
  
  std::string outfilename = cfg[ana_type][order][ana_name]["output"];
  TFile *ofile = new TFile( outfilename.c_str(), "recreate");
  for( const auto& plane : geo::planes ) {
    xymaps[plane]->Write();
    
    double max = xymaps[plane]->GetMaximum();
    double nevents = tree->GetEntries()*1.0;
    TH1F *dist = new TH1F( Form("%s_dist", xymaps[plane]->GetName()), ";Occupancy", 1000, log10(max/nevents)-6, log10(max/nevents) );
    for(int xbin=0; xbin<xymaps[plane]->GetNbinsX(); xbin++) {
      for(int ybin=0; ybin<xymaps[plane]->GetNbinsX(); ybin++) {
        dist->Fill( log10( xymaps[plane]->GetBinContent(xbin+1, ybin+1)/nevents ) );
      }
    }
    dist->Write();
    xymaps[plane]->Scale( 1.0/nevents );
    xymaps[plane]->SetName(Form("%s_mask", xymaps[plane]->GetName() ) );
    for(int xbin=0; xbin<xymaps[plane]->GetNbinsX(); xbin++) {
      for(int ybin=0; ybin<xymaps[plane]->GetNbinsX(); ybin++) {
        if( log10(xymaps[plane]->GetBinContent(xbin+1, ybin+1)) > -2.0 ) {
          xymaps[plane]->SetBinContent(xbin+1, ybin+1, 1);
        } else {
          xymaps[plane]->SetBinContent(xbin+1, ybin+1, 0);
        }
      }
    }
    xymaps[plane]->Write();
  }
  ofile->Close();
  
  for( auto& pair : xymaps ) delete  pair.second;
  
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
}


